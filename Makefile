MAKEFILE=Makefile
MAKEDEP=$(MAKEFILE)
CC	=gcc -O2 -Wall -lpcap
SRC	=main.c

# targets
all: ndprouter

install: ndprouter
	cp -f ndprouter /usr/bin/

uninstall: ndprouter
	rm /usr/bin/ndprouter

clean:
	rm -f *.o ndprouter $(OBJS)

# explicit rules
ndprouter: $(OBJS) $(MAKEDEP)
	$(CC) -ondprouter $(SRC)