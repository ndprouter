/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <pcap.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h>
#include <net/if.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <netinet/ip6.h>			/* ip6_hdr */
#include <net/ethernet.h>


#define SNAP_LEN	BUFSIZ
#define SIZE_ETHERNET	14			/* Ethernet headers are always exactly 14 bytes */
#define ETHERTYPE_IPV6	0xdd86
#define SIZE_IP6	40			/* IPv6 headers are always exactly 40 bytes */

/* MAC address structure */
typedef unsigned char s_mac_addr[6];

/* Ethernet header structure */
struct s_ethernet {
        s_mac_addr dest;			/* 48 b; destination host (MAC) address */
        s_mac_addr src;				/* 48 b; source host (MAC) address */
	unsigned short type;			/* 16 b; IP/ARP/RARP/... */
};

/* IPv6 header structure */
struct s_ip6 {
	unsigned char ver;			/*   8 b; version */
	unsigned char traffic_class;		/*   8 b; traffic class */
	unsigned short flow_label;		/*  16 b; flow label (qos) */
	unsigned short len;			/*  16 b; payload length */
	unsigned char next_header;		/*   8 b; next header */
	unsigned char hop_limit;		/*   8 b; hop limit (replaces ttl) */
	struct in6_addr	ip_src;			/* 128 b; source address */	
	struct in6_addr	ip_dest;		/* 128 b; destination address */
};

struct s_conf {
	struct in6_addr prefix;
	unsigned char len;
	unsigned char daemon;
};

struct s_conf config;

typedef struct ipv6_context {
	struct ipv6_context *next, *prev;

	struct in6_addr addr;
} ipv6_t;

ipv6_t ipv6_list;

unsigned ipv6_find (struct in6_addr *addr)
{
	ipv6_t *ipv6;

	for (ipv6 = ipv6_list.next; ipv6 != &ipv6_list; ipv6 = ipv6->next) {
		if (!memcmp (&ipv6->addr, addr, sizeof (struct in6_addr)))
			return 1;
	}

	return 0;
}

unsigned ipv6_add (struct in6_addr *addr)
{
	if (ipv6_find (addr))
		return 0;

	/* alloc and init context */
	ipv6_t *ipv6 = (ipv6_t *) malloc (sizeof (ipv6_t));

	if (!ipv6)
		return 0;

	memcpy (&ipv6->addr, addr, sizeof (struct in6_addr));

	/* add into list */
	ipv6->next = &ipv6_list;
	ipv6->prev = ipv6_list.prev;
	ipv6->prev->next = ipv6;
	ipv6->next->prev = ipv6;

	char str[64];
	char addr_src[INET6_ADDRSTRLEN];
	inet_ntop (AF_INET6, addr, addr_src, INET6_ADDRSTRLEN);

	if (config.daemon)
		printf ("> new ipv6: %s\n", addr_src);

	sprintf (str, "ip -6 neigh add proxy %s dev eth0", addr_src);

	system (str);

	return 1;
}

void handle_ipv6 (struct s_ethernet *eth, unsigned char *packet)
{
	/* define/compute IP header offset */
	struct s_ip6 *ip = (struct s_ip6 *) packet;

	/* DEBUG: print source and destination IP addresses */
#ifdef DEBUG
	char addr_src[INET6_ADDRSTRLEN];
	char addr_dest[INET6_ADDRSTRLEN];
	inet_ntop (AF_INET6, &ip->ip_src, addr_src, INET6_ADDRSTRLEN);
	inet_ntop (AF_INET6, &ip->ip_dest, addr_dest, INET6_ADDRSTRLEN);

	printf("> From: %s\n", addr_src);
	printf("> To: %s\n", addr_dest);
#endif
	if (!memcmp (&ip->ip_src, &config.prefix, config.len/8))
		ipv6_add (&ip->ip_src);
}

void handle_packet (unsigned char *args, const struct pcap_pkthdr *header, const unsigned char *packet)
{
	/* define ethernet header */
	struct s_ethernet *eth = (struct s_ethernet *) packet;
	unsigned char *payload = (unsigned char *) (packet);

	/* it could be actually whatever packet, but we assume its tunnel interface */
	return handle_ipv6 (eth, payload);
}

void daemonize (char *dev)
{
#ifndef __WIN32__
	int i, lockfd;
  	char pid[16];
	char spid[64];

	int ipid = getpid ();

  	/* detach if asked */
  	if (ipid == 1)
		return;				/* already a daemon */

    	/* fork to guarantee we are not process group leader */
    	i = fork ();

    	if (i < 0)
      		exit (1);			/* fork error */
    	if (i > 0)
      		exit (0);			/* parent exits */

    	/* child (daemon) continues */
    	setsid ();				/* obtain a new process group */
	ipid = getpid ()+1;

	printf ("> started with pid -> %d\n", ipid);
    	/* fork again so we become process group leader 
     	 * and cannot regain a controlling tty 
     	 */
    	i = fork ();

    	if (i < 0)
      		exit (1);			/* fork error */
    	else if (i > 0)
		exit (0);			/* parent exits */

    	/* close all fds */
    	for (i = getdtablesize (); i >= 0; --i)
      		close (i);			/* close all descriptors */

    	/* close parent fds and send output to fds 0, 1 and 2 to bitbucket */
    	i = open ("/dev/null", O_RDWR);

    	if (i < 0)
      		exit (1);

    	dup (i);
    	dup (i);				/* handle standart I/O */

	sprintf (spid, "%s.pid", dev);

  	/* create local lock */
  	lockfd = open (spid, O_RDWR | O_CREAT, 0640);

  	if (lockfd < 0) {
    		perror ("lock: open");
    		exit (1);
  	}
	#ifndef __CYGWIN__
	/* lock the file */
	if (lockf (lockfd, F_TLOCK, 0) < 0) {
		perror ("lock: lockf");
		printf ("> ndprouter is already running.\n");
		exit (0);
	}
	#else
	/* lock the file */
	{
	struct flock lock;
		lock.l_type = F_RDLCK;
		lock.l_start = 0;
		lock.l_whence = SEEK_SET;
		lock.l_len = 0;
		
		if (fcntl (lockfd, F_SETLK, &lock) < 0) {
			printf ("> ndprouter is already running.\n");
			exit (0);
		}
	}
	#endif
	/* write to pid to lockfile */
	snprintf (pid, 16, "%d\n", getpid ());
	write (lockfd, pid, strlen (pid));

	/* restrict created files to 0750 */
	umask (027);
#endif
}

int main (int argc, char **argv)
{
	char errbuf[PCAP_ERRBUF_SIZE];		/* error buffer */

	if (argc != 3 && argc != 4) {
		printf ("syntax: <interface> <prefix/len> [-d]\n");
		return -1;
	}

	char *dev = argv[1];			/* captured device */
	char *prefix = argv[2];

	if (argc == 4) {
		if (argv[3][0] == '-' && argv[3][1] == 'd')
			config.daemon = 1;
	} else
		config.daemon = 0;

	unsigned i;
	unsigned len = 0;
	for (i = 0; prefix[i]; i ++) {
		if (prefix[i] == '/') {
			prefix[i] = '\0';
			len = i;
			break;
		}
	}

	if (!len) {
		printf ("ERROR -> Prefix length must be specified, example: 2001:abc:def::/64\n");
		return -1;
	}

	char *plen = prefix + len + 1;

	config.len = atoi (plen);

	if (config.len < 16 || config.len > 128) {
		printf ("ERROR -> Prefix length is wrong length: 16-128 are allowed");
		return -1;
	}

	inet_pton (AF_INET6, prefix, &config.prefix);

	ipv6_list.next = &ipv6_list;
	ipv6_list.prev = &ipv6_list;

	if (!config.daemon) {
		printf ("> init\n");

		/* print capture info */
		printf ("> interface: %s\n", dev);
		printf ("> ipv6 prefix: %s/%d\n", prefix, config.len);
	} else
		daemonize (dev);

	/* open capture device */
	pcap_t *handle = pcap_open_live (dev, SNAP_LEN, 1, 1, errbuf);

	if (!handle) {
		fprintf (stderr, "ERROR -> Couldn't open device %s: %s\n", dev, errbuf);
		return -1;
	}

	/* now we can set our callback function */
	pcap_loop (handle, 0, handle_packet, NULL);

	/* free memory of pcap */
	pcap_close (handle);

	return 0;
}